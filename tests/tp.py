# Aucun n'import ne doit être fait dans ce fichier


def S(n: str) -> str:
    return "S" + n


def nombre_entier(n: int) -> str:
    if n == 0:
        return "0"

    return S(nombre_entier(n - 1))


def addition(a: str, b: str) -> str:
    if a == "0":
        return b

    return S(addition(a[1:], b))


def multiplication(a: str, b: str) -> str:
    if a == "0":
        return "0"

    return addition(b, multiplication(a[1:], b))


def facto_ite(n: int) -> int:
    res = 1

    for i in range(1, n + 1):
        res *= i

    return res


def facto_rec(n: int) -> int:
    # fact(0) = 1
    # fact(n) = n * fact(n - 1)

    if n == 0:
        return 1

    return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    # fibo(0) = 0
    # fibo(1) = 1
    # fibo(n) = fibo(n - 1) + fibo(n - 2)

    if n == 0:
        return 0

    if n == 1:
        return 1

    return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    if n == 1:
        return 1

    a, b = 0, 1

    for _ in range(n - 1):
        a, b = b, a + b

    return b


def golden_phi(n: int) -> float:
    # phi = fibo(n) / fibo(n - 1)

    if n == 0:
        raise ValueError("n doit être supérieur à 0")

    if n == 1:
        return 0.0

    return fibo_ite(n) / fibo_ite(n - 1)


def sqrt5(n: int) -> float:
    # phi = (1 + sqrt(5)) / 2
    # sqrt(5) = 2 * phi - 1

    phi = golden_phi(n)
    return 2 * phi - 1


def pow(a: float, n: int) -> float:
    # a ^ 0 = 1
    # a ^ n = | (a * a) ^ (n // 2)      , si n est pair
    #         | (a * a) ^ (n // 2) * a  , si n est impair

    if n == 0:
        return 1.0

    if n % 2 == 0:
        return pow(a * a, n // 2)

    return a * pow(a * a, n // 2)
